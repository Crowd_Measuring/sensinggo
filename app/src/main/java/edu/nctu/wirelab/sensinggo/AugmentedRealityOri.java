package edu.nctu.wirelab.sensinggo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class AugmentedRealityOri extends Activity implements LocationListener {

    SurfaceHolder sh;
    TextView spotinfo;
    LocationManager lmgr;
    Location userLocation;
    Camera camera;
    float[] values = new float[3];
    boolean blocation = false;
    private ImageView imageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_augmented_reality);
        imageView = (ImageView) findViewById(R.id.image_money);

        spotinfo = new TextView(this);
        spotinfo.setTextSize((float)24.0);
        spotinfo.setTextColor(Color.parseColor("#FF4081"));
        spotinfo.setText("Total money: " + UserConfig.getTotalMoney() +"\n");


        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        addContentView(spotinfo, params);

        SurfaceView sv = (SurfaceView)findViewById(R.id.sv);
        sh = sv.getHolder();
        sh.addCallback(new MySHCallback());

        lmgr = (LocationManager)getSystemService(LOCATION_SERVICE);
        lmgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this); // 0ms, 0m

        //spotlist.add(new Spot(24.7872268, 120.9969887, "Shine Mood NCTU"));
        Log.d("0809","on Create");
        update();
        /*if (!blocation) { // show no gps msg
            Log.d("XXX", "show no gps at beginning");
            spotinfo.setText("No GPS");
        }*/


    }

    class MySHCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            camera = Camera.open();

            if (camera == null) {
                finish();
                return;
            }

            try  {
                camera.setPreviewDisplay(holder);
            } catch (Exception e) {
                finish();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceholder) {
            if (camera == null) return;
            camera.stopPreview();
            camera.release();
            camera = null;
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceholder,
                                   int format, int w, int h) {
            camera.startPreview();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
        //update();

        Log.d("0809", "onresume ar " + UserConfig.spotlist.size());
//
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        lmgr.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0, 0, this);
        Handler handler = new Handler();
        Runnable run = new Runnable() {
            @Override
            public void run() {
               update();
               if(!UserConfig.firstMoney && blocation) { // randonm show money
                   Log.d("0808", "random show money");
                   if (showMoney()) {
                       imageView.setImageDrawable(getResources().getDrawable(R.drawable.img01));
                       imageView.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View view) {
                               imageView.setImageDrawable(null);
                               UserConfig.addMoney(1);
                               UserConfig.saveConfigTo(MainActivity.configPath);
                               spotinfo.setText("Total money: " + UserConfig.getTotalMoney() +"\n");
                               recordPosition();
                               imageView.setOnClickListener(null);
                           }
                       });

                   }
               }
            }
        };
        handler.postDelayed(run, 1500);

    }

    @Override
    protected void onPause() {
        super.onPause();
        lmgr.removeUpdates(this);lmgr.removeUpdates(this);
        //spotinfo.setText(moneyInfo+ "");
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location == null) return;
        userLocation = new Location(location);
        blocation = true;
        update();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider,
                                int status, Bundle extras) {
    }

    private boolean showMoney() {
        Random ran = new Random();
        int ranNum = ran.nextInt(10);
        if (ranNum<3) {
            return false;
        }
        else
            return true;
    }
    private void recordPosition() {
        boolean canAdd = true;
        if (UserConfig.spotlist.size()==0) {
            UserConfig.spotlist.add(new Spot(userLocation.getLatitude(), userLocation.getLongitude()) );
            return;
        }

        for (int i=0; i<UserConfig.spotlist.size(); i++) {

            // The record spot
            Spot spot = UserConfig.spotlist.get(i);
            Location spotloc = new Location(userLocation);
            spotloc.setLatitude(spot.latitude);
            spotloc.setLongitude(spot.longitude);

            float dist = userLocation.distanceTo(spotloc); // The distance between the current position and the record spot

            if (dist<=150) {
                canAdd = false;
                break;
            }

            /*if (dist>150) {
                UserConfig.spotlist.add(new Spot(userLocation.getLatitude(), userLocation.getLongitude()) );

            }*/
        }

        if (canAdd) {
            UserConfig.spotlist.add(new Spot(userLocation.getLatitude(), userLocation.getLongitude()) ) ;
        }
    }

    public class Spot {
        Spot(double latitude, double longitude, String name) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.name = name;
        }
        Spot(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
        public double latitude;
        public double longitude;
        String name;
    }

    void update() {
        String info = "";

        if (UserConfig.firstMoney && blocation) { // the first time to get the money
            imageView.setImageResource(R.drawable.img01);

            UserConfig.firstMoney = false;

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageView.setImageDrawable(null);
                    UserConfig.addMoney(1);
                    UserConfig.setFirstMoney(false);
                    UserConfig.saveConfigTo(MainActivity.configPath);
                    UserConfig.spotlist.add(new Spot(userLocation.getLatitude(), userLocation.getLongitude()));
                    spotinfo.setText("Total money: " + UserConfig.getTotalMoney() +"\n");
                    imageView.setOnClickListener(null);
                }
            });
            return;
        }

        if (!blocation) {
            Log.d("0809", "no gps");
            spotinfo.setText("Total money: " + UserConfig.getTotalMoney() +"\n"+ "No GPS");
            return;
        }
        else {
            spotinfo.setText("Total money: " + UserConfig.getTotalMoney() +"\n" + "");

        }


    }
}
