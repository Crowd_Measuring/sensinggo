package edu.nctu.wirelab.sensinggo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Random;

public class AugmentedReality extends Activity implements LocationListener {

    SurfaceHolder sh;
    TextView spotinfo;
    LocationManager lmgr;
    Location userLocation;
    Camera camera;
    float[] values = new float[3];
    float  target;
    boolean blocation = false;
    ArrayList<Spot> spotlist;
    private ImageView imageView;
    double lat, lng;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_augmented_reality);

        imageView = (ImageView) findViewById(R.id.image_money);

        //spotinfo = (TextView) findViewById(R.id.GPS_info); // can not do this
        spotinfo = new TextView(this);
        spotinfo.setTextSize((float) 24.0);
        //spotinfo.setTextColor(Color.parseColor("#FF4081"));

        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        addContentView(spotinfo, params);

        SurfaceView sv = (SurfaceView) findViewById(R.id.sv);
        sh = sv.getHolder();
        sh.addCallback(new MySHCallback());

        lmgr = (LocationManager) getSystemService(LOCATION_SERVICE);
		//lmgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this); // 0ms, 0m

//        smgr = (SensorManager)getSystemService(SENSOR_SERVICE);
//        accel = smgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        compass = smgr.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        spotlist = new ArrayList<Spot>();
        spotlist.add(new Spot(24.787667, 120.997764, "Shine Mood NCTU"));
        spotlist.add(new Spot(24.785019, 120.997397, "test position"));


        if (!blocation) { // show no gps msg
            update();
        }

        if(!UserConfig.firstMoney && blocation) {
            Log.d("0808", "random show money");
            if (showMoney()) {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.icon_svg_money));

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageView.setImageDrawable(null);
                        spotinfo.setText("");
                        UserConfig.addMoney(1);
                        UserConfig.saveConfigTo(MainActivity.configPath);
                        imageView.setOnClickListener(null);
                    }
                });

            }
        }
    }

    class MySHCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            camera = Camera.open();

            if (camera == null) {
                finish();
                return;
            }

            try {
                camera.setPreviewDisplay(holder);
            } catch (Exception e) {
                finish();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceholder) {
            if (camera == null) return;
            camera.stopPreview();
            camera.release();
            camera = null;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceholder,
                                   int format, int w, int h) {
            camera.startPreview();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();
//        smgr.registerListener(this, accel,
//                SensorManager.SENSOR_DELAY_NORMAL);
//        smgr.registerListener(this, compass,
//                SensorManager.SENSOR_DELAY_NORMAL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lmgr.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        lmgr.removeUpdates(this);
        spotinfo.setText("");
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d("0808", "locationchanged");
        if (location == null) return;
        userLocation = new Location(location);
        blocation = true;
        update();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider,
                                int status, Bundle extras) {
    }

    private boolean showMoney() {
        Random ran = new Random();
        int ranNum = ran.nextInt(10);
        if (ranNum<2) {
            return false;
        }
        else
            return true;
    }

    class Spot {
        Spot(double latitude, double longitude, String name) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.name = name;
        }
        double latitude;
        double longitude;
        String name;
    }

    void update() {
        String info = "";
        Log.d("0808", "fisrtFlag: " + UserConfig.firstMoney);

        if (UserConfig.firstMoney && blocation) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.icon_svg_money));
            UserConfig.firstMoney = false;
            // record lat,lng
            lat = userLocation.getLatitude();
            lng = userLocation.getLongitude();


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageView.setImageDrawable(null);
                    UserConfig.addMoney(1);
                    UserConfig.setFirstMoney(false);
                    UserConfig.saveConfigTo(MainActivity.configPath);

                    imageView.setOnClickListener(null);
                }
            });
            return;
        }


        if (!blocation) {
            Log.d("0808", "no gps" + blocation);
            ShowDialogMsg.showDialogShort("No GPS",500);
            return;
        }


        for (int i = 0; i < spotlist.size(); i++) {
            final int tmpIndex = i;

            Spot spot = spotlist.get(i);
            Location spotloc = new Location(userLocation);
            spotloc.setLatitude(spot.latitude);
            spotloc.setLongitude(spot.longitude);

            target = userLocation.bearingTo(spotloc);

            float dist = userLocation.distanceTo(spotloc);
            if (dist > 150.0) {
                //imageView.setImageDrawable(null);
                continue;
            }
            else{
                imageView.setImageResource(R.drawable.img01);
                //imageView.setImageDrawable(getResources().getDrawable(R.drawable.icon_svg_money));
                info = info + spot.name + "\n" + dist + "公尺\n";

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageView.setImageDrawable(null);
                        spotlist.remove(tmpIndex);
                        spotinfo.setText("");
                        UserConfig.addMoney(1);
                        UserConfig.saveConfigTo(MainActivity.configPath);
                        imageView.setOnClickListener(null);
                    }
                });
            }


        }
        spotinfo.setText(info);
    }

}
