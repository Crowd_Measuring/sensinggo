package edu.nctu.wirelab.sensinggo.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.nctu.wirelab.sensinggo.Connect.HttpsConnection;
import edu.nctu.wirelab.sensinggo.File.JsonParser;
import edu.nctu.wirelab.sensinggo.R;
import edu.nctu.wirelab.sensinggo.UserConfig;

/**
 * Created by py on 4/9/18.
 */

public class SocialFragment extends Fragment{
    private ListView listView;

    //Context mContext;
    SimpleAdapter adapter;
    List<Map<String, Object>> items;
    List<Object> imageList;

    private JsonParser jsonParser = null;
    public void setJsonParser(JsonParser json) {
        jsonParser = json;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        Log.d("0715", "arraylen");

        Log.d("0715len", Integer.toString(UserConfig.similarityArray.length));
        if (UserConfig.similarityArray.length==1) {
            View view = inflater.inflate(R.layout.textview_nodata, container, false);
            TextView tv = (TextView) view.findViewById(R.id.info_text) ;
            tv.setText("new user has no data yet");
            return view;
        }

        else{
            View view = inflater.inflate(R.layout.listview_social, container, false);

            listView = (ListView) view.findViewById(R.id.list);

            items = new ArrayList<Map<String, Object>>();
            imageList = new ArrayList<Object>();
            imageList.add(R.drawable.icon_manb);
            imageList.add(R.drawable.icon_mang);
            imageList.add(R.drawable.icon_mano);
            imageList.add(R.drawable.icon_many);
            imageList.add(R.drawable.icon_manr);


            for (int i=0; i< UserConfig.similarityArray.length; i++){
                Map<String, Object> item = new HashMap<String, Object>();

                Log.d("0613", UserConfig.similarityArray[i][0]);

                item.put("userIcon", imageList.get(i));
                //item.put("userName", UserConfig.similarityArray[i][0]);
                item.put("userEmail", UserConfig.similarityArray[i][1]);
                items.add(item);
            }


            adapter = new SimpleAdapter(
                    getActivity(),
                    items,
                    R.layout.fragment_social,
                    new String[]{"userIcon", "userEmail"},
                    new int[]{R.id.similarIcon, R.id.UserEmail}
                    //new String[]{"userIcon", "userName", "userEmail"},
                    //new int[]{R.id.similarIcon, R.id.UserId, R.id.UserEmail}
            );


            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public  void onItemClick(AdapterView<?> parent, View view, int position, long id){

                    String name = UserConfig.similarityArray[position][0];
                    String info = "username=" + name;
                    Log.d("0614pos", Integer.toString(position) + '\t'+ name);
                    connectServer("POST", "/getUserInfo", info);
                }
            });

            return  view;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }

    public void connectServer(String method, String path, String info){
        Log.d("0612conn", info);
        HttpsConnection httpsConnection = new HttpsConnection(getActivity());
        httpsConnection.setJsonParser(jsonParser);
        httpsConnection.setActivity(getActivity());
        httpsConnection.setMethod(method, info);
        httpsConnection.execute(path);

    }
}
